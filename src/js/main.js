import { SectionCreator } from './components/SectionCreator.js';
import '../styles/style.css';
import './components/WebsiteSection';

document.addEventListener('DOMContentLoaded', () => {
  SectionCreator.create('community').render();
  SectionCreator.create('standard').render();
});
