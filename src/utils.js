export function makeRequest(endpoint, method, payload) {
  const initObj = {
    method: method,
    headers: {
      'Access-Control-Allow-Origin': 'http://localhost:8080',
      'Content-Type': 'application/json'
    },
    mode: 'cors' };
  if (method !== 'GET') {
    initObj.body = JSON.stringify(payload);
  }

  return fetch(endpoint, initObj)
      .then(response => response.json().then(responseData => ({ status: response.status, responseData })))
      .then(data => {
        if (data.status === 422) {
          alert(data.responseData.error);
        }
        return data;
      })
      .catch(error => console.log(error)); // eslint-disable-line no-console
}

export function removeSection(className) {
  const section = document.getElementsByClassName(className)[0];
  section.parentNode.removeChild(section);
}

export function createElement(elemType, classNames, innerText) {
  const elem = document.createElement(elemType);

  if (classNames) {
    Array.isArray(classNames) ? elem.classList.add(...classNames) : elem.classList.add(classNames);
  }

  if (innerText) {
    elem.innerText = innerText;
  }

  return elem;
}

export function addNewElement(parentElem, elemType, classNames, innerText) {
  parentElem.appendChild(createElement(elemType, classNames, innerText));
}
